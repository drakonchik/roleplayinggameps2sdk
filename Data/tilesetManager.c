#include "include/tilesetManager.h"
#include <stdio.h>
GSTEXTURE Tileset;

u64 TilesetCol;

Tiles TownTileset[30];

u16 tileWidth;
u16 tileHeight;

extern char* root;

void InitializeTileset(GSGLOBAL* gsGlobal)
{
	char* tempRoot = "";
	strncpy(tempRoot, root, strlen(root));
	gsKit_texture_png(gsGlobal, &Tileset, strcat(tempRoot,"Graphics/Tilesets/TilesetTown.png"));
	TilesetCol = GS_SETREG_RGBAQ(0x80,0x80,0x80,0x80,0x00);
	tileWidth = 32;
	tileHeight = 32;
	
	s32 w = 0;
	for(u16 i = 0; i < 6; i++)
	{
		for(u16 j = 0; j <= 4; j++)
		{
			TownTileset[w].tileX = j * tileWidth; // 0 16 32 48 64 80 
			TownTileset[w].tileY = i * tileHeight;
			//printf("%d , %d Tile %d\n",TownTileset[w].tileX,TownTileset[w].tileY,w);
			TownTileset[w].ID = 0; // Rn all tiles are ground, lazy to change
			w++;
		}
	
	}
}

void UpdateTileset(GSGLOBAL *gsGlobal)
{
	// Some Tilesets have moving parts such as water... sooooo
}

void DrawTile(GSGLOBAL *gsGlobal, u64 colour, u16 tileID, u16 posX, u16 posY)
{			
	
	gsKit_prim_sprite_texture(gsGlobal, &Tileset,posX,  // X1
						posY,  // Y2
						TownTileset[tileID].tileX,  // U1
						TownTileset[tileID].tileY,  // V1
						tileWidth + posX, // X2
						tileHeight + posY, // Y2
						TownTileset[tileID].tileX + tileWidth, // U2
						TownTileset[tileID].tileY + tileHeight, // V2			
						2,
						TilesetCol);
}




