#include "include/gameScreen.h"
#include "include/menuScreen.h"
#include "include/overScreen.h"

#include "include/pad.h"
#include "include/debugMAP.h"
#include "include/Player.h"

#include <stdio.h>
#include <malloc.h>

#include <gsKit.h>
#include <dmaKit.h>
#include <gsToolkit.h>
extern StateMachine GameMachineState;

extern Controller PlaystationGamePad;

extern PlayerCharacter player;

u16 xOff = 0;
u16 yOff = 0;

void GameStart(GSGLOBAL* gsGlobal)
{
	//printf("Initialising Game State\n");
	InitializeMap(gsGlobal);
	InitializePlayer(gsGlobal);
}

void GameUpdate(GSGLOBAL* gsGlobal)
{
		
	UpdateMap(gsGlobal);
	//gsKit_set_display_offset(gsGlobal, player.posX-320, player.posY-256);
	//gsGlobal->OffsetX = player.posX-320;
	//gsGlobal->OffsetY = player.posY-256;
	
	//gsGlobal->StartX = player.posX-320;
	//gsGlobal->StartY = player.posY-256;
}

void GameDraw(GSGLOBAL* gsGlobal, u64 colour)
{
	// This part here handles thee map that's being drawn.
	DrawMap(gsGlobal,colour, xOff, yOff);
}

void GameEnd(GSGLOBAL* gsGlobal)
{
	// Mandatory Cleanup of the VRAM when changing state
	gsKit_vram_clear(gsGlobal);
}

StateManager GameState =
{
	GameStart,
	GameUpdate,
	GameDraw,
	GameEnd
};
